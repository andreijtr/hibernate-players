/**
 * relatia One-to-One:
 * owner-side e player, playerProfile e inverse-side sau referencing side
 * owner-side contine FK adica @JoinColumn, iar inverse-side contine "mappedBy", dar merge si fara mappedBy
 * cand persist playerul, automat persista si profilul sau daca e setat
 */


package com.sda.hibernate.players.player;

import com.sda.hibernate.players.playerProfile.PlayerProfile;
import com.sda.hibernate.players.team.Team;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "players")
public class Player {

    private static final String PLAYER_GENERATOR = "player_generator";
    private static final String PLAYER_SEQUENCE = "player_sequence";


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PLAYER_GENERATOR)
    @SequenceGenerator(name=PLAYER_GENERATOR, sequenceName = PLAYER_SEQUENCE)
    private int id;


    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "player_profile_id", referencedColumnName = "id", unique = true)
    private PlayerProfile playerProfile;

    @ManyToOne
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;

    public Player() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return id == player.id &&
                Objects.equals(firstName, player.firstName) &&
                Objects.equals(lastName, player.lastName) &&
                Objects.equals(playerProfile, player.playerProfile) &&
                Objects.equals(team, player.team);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, playerProfile, team);
    }

    public Player(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public PlayerProfile getPlayerProfile() {
        return playerProfile;
    }

    public void setPlayerProfile(PlayerProfile playerProfile) {
        this.playerProfile = playerProfile;
    }

    public Team getTeam() {
        return team;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", playerProfile=" + playerProfile +
                ", team=" + team +
                '}';
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
