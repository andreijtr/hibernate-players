package com.sda.hibernate.players.player;

import com.sda.hibernate.players.database.DbInitializer;
import com.sda.hibernate.players.player.Player;

public class PlayerDao extends DbInitializer {

    public PlayerDao() {}

    public void insertPlayer(Player player) {
        //open session and transaction
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();

        //from here begins the transactions (operations with the db)
        session.save(player);

        //now program close the transaction and session
        transaction.commit();
        session.close();

    }

    public Player findPlayerById(int id) {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();

        Player player = session.get(Player.class, id);

        transaction.commit();
        session.close();

        return player;
    }
}
