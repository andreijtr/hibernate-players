package com.sda.hibernate.players.league;

import com.sda.hibernate.players.team.Team;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "leagues")
public class League {

    private static final String LEAGUE_GENERATOR = "league_generator";
    private static final String LEAGUE_SEQUENCE = "league_sequence";


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = LEAGUE_GENERATOR)
    @SequenceGenerator(name= LEAGUE_GENERATOR, sequenceName = LEAGUE_SEQUENCE)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE})
    @JoinTable(name = "teams_leagues",
            joinColumns = @JoinColumn(name = "league_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id"))
    private Set<Team> teams;

    public League() {
        teams = new HashSet<>();
    }

    public League(String name) {
        this();
        this.name = name;
    }

    //metode utilitare de adaugare echipe
    public void addTeam(Team team){
        teams.add(team);
        team.getLeagues().add(this);
    }

    public void removeTeam(Team team) {
        teams.remove(team);
        team.getLeagues().remove(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
