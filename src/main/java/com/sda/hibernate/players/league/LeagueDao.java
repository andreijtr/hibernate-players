package com.sda.hibernate.players.league;

import com.sda.hibernate.players.database.DbInitializer;

public class LeagueDao extends DbInitializer {
    public void insertLeague(League league) {
        //open session and transaction
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();

        //from here begins the transactions (operations with the db)
        session.save(league);

        //now program close the transaction and session
        transaction.commit();
        session.close();

    }
}
