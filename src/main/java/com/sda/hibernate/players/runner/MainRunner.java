package com.sda.hibernate.players.runner;

import com.sda.hibernate.players.league.League;
import com.sda.hibernate.players.league.LeagueDao;
import com.sda.hibernate.players.player.PlayerDao;
import com.sda.hibernate.players.player.Player;
import com.sda.hibernate.players.playerProfile.PlayerProfile;
import com.sda.hibernate.players.team.Team;
import com.sda.hibernate.players.team.TeamDao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class MainRunner {

    public static void main(String[] args) {

        PlayerDao playerDao = new PlayerDao();
        TeamDao teamDao = new TeamDao();
        LeagueDao leagueDao = new LeagueDao();

        //create players
        Player playerVasile = new Player("vasile", "lupu");
        Player playerHagi = new Player("gica", "hagi");
        Player playerTudose = new Player("mihai", "tudose");

        //create profiles
        PlayerProfile vasileProfile = new PlayerProfile(3,2);
        PlayerProfile hagiProfile2 = new PlayerProfile(0,10);
        PlayerProfile tudoseProfile = new PlayerProfile(2,2);

        //set players profile
        playerVasile.setPlayerProfile(vasileProfile);
        playerHagi.setPlayerProfile(hagiProfile2);
        playerTudose.setPlayerProfile(tudoseProfile);

        vasileProfile.setPlayer(playerVasile);
        hagiProfile2.setPlayer(playerHagi);
        tudoseProfile.setPlayer(playerTudose);

        //create team and set players
        Team steauaTeam = new Team("FCSB");
        Team barcelonaTeam = new Team("Barcelona");

        steauaTeam.addPlayer(playerVasile);
        steauaTeam.addPlayer(playerHagi);

        barcelonaTeam.addPlayer(playerTudose);

        //teamDao.insertTeam(steauaTeam);

        //create leagues
        League championsLeague = new League("Champions League");
        League eufaLeague = new League("UEFA league");
        League liga1 = new League("liga 1");

        //set teams to champions leagues
        championsLeague.addTeam(steauaTeam);
        championsLeague.addTeam(barcelonaTeam);

        //set teams to liga1
        liga1.addTeam(steauaTeam);

        //persist teams
        teamDao.insertTeam(steauaTeam);
        teamDao.insertTeam(barcelonaTeam);

        //persist league
        leagueDao.insertLeague(championsLeague);
        leagueDao.insertLeague(liga1);
    }
}
