package com.sda.hibernate.players.playerProfile;

import com.sda.hibernate.players.player.Player;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "players_profile")
public class PlayerProfile {

    private static final String PROFILE_SEQUENCE = "profile_id_sequence";
    private static final String PROFILE_GENERATOR = "profile_generator";

    @Id
    @SequenceGenerator(name = PROFILE_GENERATOR, sequenceName = PROFILE_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PROFILE_GENERATOR)
    private int id;

    @Column(name = "goals_scored")
    private Integer goalsScored;

    @Column(name = "games_played")
    private Integer gamesPlayed;

    @OneToOne(mappedBy = "playerProfile")
    private Player player;

    //mappedBy ii spune Hibernate -ului ce variabila folosim din entitatea asociata(owning-side), pentru a mapa aceasta entitate

    public PlayerProfile() {
    }

    public PlayerProfile(Integer goalsScored, Integer gamesPlayed) {
        this();
        this.goalsScored = goalsScored;
        this.gamesPlayed = gamesPlayed;
    }

    //am sters campul player din functiile hashCode si equals pt ca se apelau recursiv in relatia OneToMany si nu inteleg dc
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlayerProfile)) return false;
        PlayerProfile that = (PlayerProfile) o;
        return id == that.id &&
                Objects.equals(goalsScored, that.goalsScored) &&
                Objects.equals(gamesPlayed, that.gamesPlayed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, goalsScored, gamesPlayed);
    }

    @Override
    public String toString() {
        return "PlayerProfile{" +
                "id=" + id +
                ", goalsScored=" + goalsScored +
                ", gamesPlayed=" + gamesPlayed +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getGoalsScored() {
        return goalsScored;
    }

    public void setGoalsScored(Integer goalsScored) {
        this.goalsScored = goalsScored;
    }

    public Integer getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(Integer gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
