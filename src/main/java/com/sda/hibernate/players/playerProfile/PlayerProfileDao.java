package com.sda.hibernate.players.playerProfile;

import com.sda.hibernate.players.database.DbInitializer;
import com.sda.hibernate.players.playerProfile.PlayerProfile;

public class PlayerProfileDao extends DbInitializer {

    public void insertPlayerProfile (PlayerProfile playerProfile) {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();

        session.save(playerProfile);

        transaction.commit();
        session.close();
    }
}
