package com.sda.hibernate.players.team;

import com.sda.hibernate.players.database.DbInitializer;

public class TeamDao extends DbInitializer {

    public void insertTeam(Team team) {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();

        session.save(team);

        transaction.commit();
        session.close();
    }
}
