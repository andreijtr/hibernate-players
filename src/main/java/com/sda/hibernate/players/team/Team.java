package com.sda.hibernate.players.team;


import com.sda.hibernate.players.league.League;
import com.sda.hibernate.players.player.Player;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "teams")
public class Team {

    private static final String TEAM_GENERATOR = "team_generator";
    private static final String TEAM_SEQUENCE = "team_sequence";


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TEAM_GENERATOR)
    @SequenceGenerator(name=TEAM_GENERATOR, sequenceName = TEAM_SEQUENCE)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "team", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Player> players;

    //mappedBy ii spune Hibernate-ului ce variabila folosim in entitatea asociata, pt a mapa aceasta relatie. Se refera la atributul din Java si NU la coloana din tabel

    //@OneToMany in aceasta configuratie este o relatie bidirectionala. Cnd persistam echipa, se salveaza si jucatorii cu profilele lor

    @ManyToMany(mappedBy = "teams")
    private Set<League> leagues;

    public Team() {
        players = new HashSet<>();
        leagues = new HashSet<>();
    }

    public Team(String name) {
        this();
        this.name = name;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
    //pentru ca vrem sa persistem jucatorii, atunci cand adaugam echipa folosim urmatoarea metoda
    //cand adaugam un jucator in echipa, ii setam si lui echipa

    public void addPlayer(Player player) {
        players.add(player);
        player.setTeam(this);
    }
    //cand stergem un jucator din echipa, ii setam si atributul sau "team" ca fiind null

    public void removePlayer(Player player) {
        players.remove(player);
        player.setTeam(null);
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(Set<League> leagues) {
        this.leagues = leagues;
    }
}
